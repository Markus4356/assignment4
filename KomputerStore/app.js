// Number format
let NOK = new Intl.NumberFormat('no-NO', {
    style: 'currency',
    currency: 'NOK',
  });

/*
Code for the Bank, Work and Buy
*/

// Variables
let balance = 0
let loan = 0
let pay = 0
let haveLoan = false

// DOM elements
const btnLoan = document.getElementById("Loan")
const btnRepay = document.getElementById("Repay")
const btnBank = document.getElementById("Bank")
const btnWork = document.getElementById("Work")
const btnBuy = document.getElementById("Buy")

updateValues()

// Handlers
btnLoan.addEventListener("click", btnTakeLoan)
btnRepay.addEventListener("click", btnRepayLoan)
btnBank.addEventListener("click", btnBankMoney)
btnWork.addEventListener("click", btnWorkMoney)
btnBuy.addEventListener("click", btnBuyLaptop)


// Functions
function updateValues(){
    document.getElementById("Balance").innerHTML = "Balance: " + NOK.format(balance)
    document.getElementById("Pay").innerHTML = "Pay: " + NOK.format(pay)
    
    if(loan == 0){
        document.getElementById("loanAmount").innerHTML = null
        haveLoan = false
        hideElement("Repay", true)
    }
    else{
        document.getElementById("loanAmount").innerHTML = "Loan: " + NOK.format(loan)
        haveLoan = true
        hideElement("Repay", false)
    }
}

function hideElement(element, hide){
    if (hide){
        document.getElementById(element).style.display = "none"
    }
    else{
        document.getElementById(element).style.display = "block"
    }
}

// Button Functions
function btnTakeLoan(){
    loanValue = prompt("Take a loan?")
    if(balance*2 >= loanValue && !haveLoan && loanValue > 0){
        loan += +loanValue
        balance += +loanValue

        updateValues()
    }
    else
        alert("Cannot loan")
}

function btnRepayLoan(){
    if(loan >= pay){
        loan -= pay
    }
    else{
        balance += pay-loan
        loan = 0
    }

    pay = 0
    updateValues()
}

function btnBankMoney(){
    if(haveLoan && (pay*0.1)<= loan){
        balance = balance+(pay*0.9)
        loan = loan-(pay*0.1)
    }
    else if(haveLoan){
        balance += (pay - loan)
        loan = 0
    }
    else{
        balance += pay
    }

    pay = 0
    updateValues()
}

function btnWorkMoney(){
    pay += 100
    updateValues()
}

function btnBuyLaptop(){
    if(price <= balance){
        alert(`You have bought ${laptopName} for ${NOK.format(price)}!`)
        balance = balance - price
        updateValues()
    }
    else{
        alert('This laptop is too expensive!')
    }
}


/*
Code for the Laptop sections
*/

// Variables
let price = 0
let laptopName = ""

// DOM Elements
const laptopsSelectElement = document.getElementById("laptops")
const laptopsTitleElement = document.getElementById("laptop-title")
const laptopsDescriptionElement = document.getElementById("laptop-description")
const laptopsSpecsElement = document.getElementById("laptop-specs")
const laptopPriceElement = document.getElementById("laptop-price")
const laptopsImageElement = document.getElementById("laptop-image")

const laptops = []

// Laptop API
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
.then((response) => {
    return response.json()
})
.then((jsonLaptops) => {
    laptops.push(...jsonLaptops)
    renderLaptops(jsonLaptops)
})

//Hide the laptop section and features header when no laptop is displayed
hideElement("laptopSection", true)
hideElement("features", true)

//Functions
function renderLaptops(laptops) {
    for(const laptop of laptops){
        laptopsSelectElement.innerHTML += `<option value= ${laptop.id}> ${laptop.title}</option>`
    }
}

function renderLaptop(selectedLaptop) {
    laptopsImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedLaptop.image
    laptopsTitleElement.innerText = `${selectedLaptop.title}`
    laptopsDescriptionElement.innerText = `${selectedLaptop.description}`
    laptopPriceElement.innerText = `Price: ${NOK.format(selectedLaptop.price)}`

    laptopsSpecsElement.innerHTML = ""
    for (const key in selectedLaptop.specs){
        const specs = selectedLaptop.specs[key]
        laptopsSpecsElement.innerHTML += `<li>${specs}</li>`
    }

    price = selectedLaptop.price
    laptopName = selectedLaptop.title

}

function onSelectChange(){
    const laptopId = this.value
    const selectedLaptop = laptops.find(g => g.id == laptopId)

    hideElement("laptopSection", false)
    hideElement("features", false)
    renderLaptop(selectedLaptop)
}

laptopsSelectElement.addEventListener("change", onSelectChange)